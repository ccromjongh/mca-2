import os
base_dir = '/home/user/workspace/assignment1'
import re
import numpy as np


def set_work_dir(relative=''):
    """
    Sets the work directory to base_dir + relative
    @param (str) relative: The relative path to set the work directory to
    """
    new_dir = base_dir + relative
    os.chdir(new_dir)


def load_csv(file_name):
    f = open(base_dir+'/'+file_name+'.txt', 'r')
    # f1 = f.readlines()
    # f1 = f.read().splitlines()
    # data = []
    # for line in f1:
    #     data.append(line.split(','))
    
    # Split the csv file into separate data-chunks 
    d1 = re.split(',|\n', f.read())
    d1.pop()  # Remove last newline
    column_width = 12
    n = len(d1)
    n_rows = int(n/column_width)
    data = np.array(d1)\
        .reshape((n_rows, column_width))\
        .astype(int)
    return data

def save_state(cur_config, job1, job2, file_name):
    f = open(base_dir+'/'+file_name+'.txt', 'a+')
    f.write(cur_config.config_to_string() + ',{0},{1},{2}\n'
            .format(job1.get_exec_cycles(), job2.get_exec_cycles(), cur_config.area))
    f.close()
    
