from common import base_dir, set_work_dir
import os
import math
from job_cls import Job


class Configuration:
    """
    Configuration class that allows you to create and run a certain processor configuration,
     and acquire simulation results.
    """
    jobs = {}

    def __init__(self, issue_width, mpy0, name='temp'):
        """
        Initialise this configuration with the given parameters

        Args:
            issue_width (int): Issue width of the processor. At least 2, must be power of 2
            mpy0 (int): # of multiplier syllables per cycle. Must be at least 1 and at most issue_width
            name (str): The name of this configuration
        """
        if math.log2(issue_width) % 1 > 0:
            raise Exception("The issue_width should be a power of two. {0} was given".format(issue_width))
        self.issue_width = issue_width
        if mpy0 > issue_width:
            raise Exception("# of mpy's must be <= the issue_width ({0} vs {1}".format(mpy0, issue_width))
        self.mpy0 = mpy0
        self.name = name

        # Create directory with the name of the configuration.
        if not os.path.exists(self.abs_path):
            os.mkdir(self.abs_path)

    @classmethod
    def from_list(self, params, name='temp'):
        return self(params[0], params[1], name)

    @property
    def abs_path(self):
        """
        Returns the path that contains the configuration
        """
        return base_dir + '/configurations/' + self.name

    @property
    def rel_path(self):
        return '/configurations/' + self.name

    def config(self):
        """
        Returns:
            dict: Dictionary with config parameters
        """
        return {'issue_width': self.issue_width, 'mpy0': self.mpy0}

    def create_job(self, name):
        """
        Returns a job simulation
        Args:
            name (str): Name of the job
        Returns:
            Job: Job class instance
        """
        # if not self.jobs.get(name, None):
        job = Job(name, self.rel_path)
        self.jobs[name] = job
        return job

    def get_job(self, name):
        """
        Creates a job simulation
        Args:
            name (str): Name of the job
        Returns:
            Job: Job class instance
        """
        return self.jobs.get(name, None)

    def get_config_string(self):
        config_template = """
# The r-VEX can be configured to have an issue width of 2, 4, or 8. Each issue
# slot has an ALU, so the number of ALUs must be set to the issue width.
RES: IssueWidth     {0}
RES: IssueWidth.0   {0}
RES: Alu.0          {0}

# The number of multipliers. This can be freely configured between 1 and the
# issue width.
RES: Mpy.0          {1}

RES: Memory.0       1
RES: MemLoad        1
RES: MemStore       1
RES: MemPft         0
# ***Clusters***    1
RES: CopySrc.0      0
RES: CopyDst.0      0
REG: $r0            63
REG: $b0            8
DEL: AluR.0         0
DEL: Alu.0          0
DEL: CmpBr.0        0
DEL: CmpGr.0        0
DEL: Select.0       0
DEL: Multiply.0     1
DEL: Load.0         1
DEL: LoadLr.0       1
DEL: Store.0        0
DEL: Pft.0          0
DEL: CpGrBr.0       0
DEL: CpBrGr.0       0
DEL: CpGrLr.0       0
DEL: CpLrGr.0       0
DEL: Spill.0        0
DEL: Restore.0      1
DEL: RestoreLr.0    1
CFG: Quit           0
CFG: Warn           0
CFG: Debug          0"""

        config_string = config_template.format(self.issue_width, self.mpy0)
        return config_string

    def write_config(self):
        """
        Creates the config string and writes it to the disk as configuration.mm in the
         config folder defined by the config name in __init__
        """
        set_work_dir(self.rel_path)

        # Create configuration string using the parameters set in the __init__
        config_string = self.get_config_string()
        # Write configuration
        f = open('configuration.mm', 'w')
        f.write(config_string)
        f.close()
        set_work_dir()

    def config_to_string(self):
        return "{0},{1}".format(self.issue_width, self.mpy0)

    @property
    def area(self):
        """
        Calculates the approximate area of the configuration
        """
        alu = self.issue_width
        mult = self.mpy0
        lwsw = 1  # think this is the right variable
        gr = 63
        br = 8
        conn = 1 + 1 + 0  # sums all the connections to the cache (MemLoad + MemStorem + MemPft)
        issuewidth = self.issue_width

        area_alu  =  3273
        area_mult = 40614
        area_lwsw =  1500
        area_64   = 26388
        area_8    =   258
        area_misc =  6739  # isn't used but it is given
        area_conn =  1000

        # this formula is given, don't know were the "/4" comes from
        area_gr = area_64 / 64 * gr * math.sqrt(issuewidth / 4)

        # not sure if the "/4" needed here
        area_br = area_8 / 8 * br * math.sqrt(issuewidth / 4)

        area = alu * area_alu + mult * area_mult + lwsw * area_lwsw + area_gr + area_br + conn * area_conn

        return int(area)
