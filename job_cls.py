from common import base_dir, set_work_dir
import os
import subprocess
import signal
import time
from IPython.display import Image
import re


class Job:
    """
    Runs job etc
    """

    def __init__(self, name, path):
        """
        Initialise job
        Args:
            name (str): Name of the job
            path (str): Path of the configuration folder
        """
        self.name = name
        self.conf_path = path

    @property
    def abs_path(self):
        """
        Returns the path that contains the configuration
        """
        return base_dir + '/{}/output-{}.c'.format(self.conf_path, self.name)

    @property
    def rel_path(self):
        return '{}/output-{}.c'.format(self.conf_path, self.name)

    def run(self, flags='-O3'):
        """
        Run the job using the flags provided
        Args:
            flags (str): Compiler optimisation flags
        """
        set_work_dir(self.conf_path)
        run_out = subprocess.Popen(['run', self.name, flags],
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
        # This waits for the process to finish
        stdout, stderr = run_out.communicate()
        stdout = str(stdout).split('\\n')
        stderr = str(stderr).split('\\n')
        set_work_dir()
        return stdout, stderr

    def get_image(self, dpi=100, timeout=0.3):
        """
        Args:
            dpi (int): DPI of the image that is generated
            timeout (int): How long to wait for the generation of the vcg file in seconds
        Returns:
            str: The path to the image
        """
        set_work_dir(self.rel_path)

        rgg_out = subprocess.Popen(['rgg', 'a.out', '-g', 'gmon-nocache.out'],
                                   preexec_fn=os.setsid)
        # Small time-out to generate file.
        time.sleep(timeout)

        # Run command to create graph image
        os.system('echo gcall.vcg | vcg -ppmoutput gcall.ppm -xdpi {0} -ydpi {0}'.format(dpi))
        # Kill rgg process by sending the SIGTERM signal to all the process groups
        os.killpg(os.getpgid(rgg_out.pid), signal.SIGTERM)

        # Convert image to png and remove ppm file
        os.system('pnmtopng gcall.ppm > gcall.png')
        os.system('rm gcall.ppm')
        set_work_dir()
        return self.abs_path + '/gcall.png'

    def disp_image(self):
        """
        Returns:
            Image: Image instance that can be displayed in Jupyter Notebook
        """
        return Image(filename=self.abs_path + '/gcall.png')

    def get_gmon(self):
        """
        Get and parse gmon performance file
        Returns:
            list: List of functions, sorted by execution time, with their properties
        """
        set_work_dir(self.rel_path)
        # Generate gmon text file
        os.system('gprof a.out gmon-nocache.out > gmon-nocache.txt')
        f = open('gmon-nocache.txt', 'r')
        f1 = f.readlines()
        functions = []

        for i in range(5, len(f1)):
            line = f1[i]
            search = re.search('([0-9.]+) +([0-9.]+) +([0-9.]+) +([0-9.]+) +([0-9.]+) +([0-9.]+) +(\w+)', line)
            if not search:
                break
            result = {'time': float(search.group(1)), 'cumulative': float(search.group(2)),
                      'self': float(search.group(3)),
                      'calls': int(search.group(4)), 'self_ms': float(search.group(5)),
                      'total_ms': float(search.group(6)),
                      'name': search.group(7)}
            functions.append(result)

        set_work_dir()
        return functions

    def get_prof_file(self, type='static', filter=True):
        """
        Get profile file
        Args:
            type (str): Either `static` or `dyn`
            filter (bool): Whether or not to filter out operations that are not called
        Returns:
            list: List of operators, with properties name and # of executions
        """
        set_work_dir(self.rel_path)
        f = open('{}_prof_file.txt'.format(type), 'r')
        f1 = f.readlines()
        functions = []

        for line in f1:
            search = re.search('operation:[ \t]+(\w+)[ \t]+n\. times:[ \t]+([0-9]+)', line)
            if not search:
                break
            operation = search.group(1)
            n_times = int(search.group(2))
            if filter and n_times == 0:
                continue
            result = {'operation': operation, 'n_times': n_times}
            functions.append(result)

        set_work_dir()
        return functions

    def get_exec_cycles(self):
        """
        Extract the execution cycle count
        Returns:
            int: Number of execution cycles for this job
        """
        set_work_dir(self.rel_path)
        f = open('ta.log.000', 'r')
        f.readline()  # Skip line
        line = f.readline()
        search = re.search('([0-9]+)', line)
        if not search:
            return None

        cycles = int(search.group(1))

        set_work_dir()
        return cycles